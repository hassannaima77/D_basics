import 'dart:math';


// void main(){
//   print('My lil babay');
// }


// numbers = (int,float) 1, 10.0
// string = 'Naima'
// boolean = true or false
// lists = (array)List<int>
// maps = hashmaps 
// runes = unicode character set Runes("123")
// symbols = #sysmbols
//function = (){}
// num is a parent to int and double
// == Equal to
// != Not equal to
// > Greater than
// < Less than
// > = Greater than or equal to
// < = Less than or equal to
// ! Negation


// void main() {
  

//   int x = 10;
//   double y = 5.0;

//   String s = "${x+y}"; //string intrapoletion
//   print(s);

//   bool b = true;
//   print(b);

//   List l = [1,2,3];
//   print(l[0]);

//   List<String> ls = ['1','2','3'];
//   print(ls[2]);

//   Map me = {
//     'Name' : 'Naima',
//     'Age' : 22,
//     'Type' : 'Gay',
//   };
//   print(me['Type']);
// }



// int add(int a, int b){
//   return a + b;
// }

// Function fun;

// void main(){
//   fun = add;

//   var result = fun(20,30);
//   print('Result is ${result}');
// }


// int add(int a, int b){
//   return a + b;
// }

// exec(Function op, x, y){
//   return op(x, y);
// }


// void main(){
//   var result = exec(add, 20, 30);
//   print('Result is still ${result} ');
// }



// int add(int a, int b) => a + b ;
// int sub(int a, int b) => a - b ;

// choose(bool op){
//   if(op == true){
//     return add;
//   }
//   else{
//     return sub;
//   }
// }

// void main(){
//   var result = choose(false);
//   print('Result is ${result(40,30)} ');
// }



// int add(int a, int b) => a + b ;
// int sub(int a, int b) => a - b ;

// List operators = [add, sub];

// void main(){
//   var result = operators[0](10,20);
//   print('Result is ${result}');
// }


// void main(){
//   int x = 10; //initializing
//   while (x > 5){ //condtion
//     print("$x");
//     x --; //decrament
//   }
// }



// void main(){
//   int x = 10; 
//   do {
//     print('$x');
//     x --;
//   }
//   while (x > 1);
// }

// void main(){
//   for (int x = 10; x > 3; x--){
//     print('$x');
//   }
// }


//after importing math library from dart

void main(){
  Square square = Square(10.0);
  Rectangle rectangle = Rectangle(20.0, 10.0);
  Circle circle = Circle(2.0);

  print(square.name);
  print(rectangle.name);
  print(circle.name);
}

abstract class Shape{
  double get perimeter;
  double get area;
  String get name;
}

class Circle extends Shape{
  double radius;

  Circle(this.radius);

  @override
  // TODO: implement area
  double get area => pi * (radius*radius);

  @override
  // TODO: implement name
  String get name => "I am a circle with radius $radius";

  @override
  // TODO: implement perimeter
  double get perimeter => radius * 2 *pi;

}


class Rectangle extends Shape{
  double length, width;

  Rectangle(this.length,this.width);

  @override
  // TODO: implement area
  double get area => length * width;

  @override
  // TODO: implement name
  String get name => "I am a rectangle with length: $length and width: $width";

  @override
  // TODO: implement perimeter
  double get perimeter => length * 2 + width * 2;

}

class Square extends Rectangle{
  Square(
    double side,
    ) : super(
      side,
      side,
       );
       @override
  // TODO: implement name
  String get name => "I am a square with side of: $length";
 

}

